## Rx JS
`shimaore.gitlab.io/ready-prose`

`github.com/BrestJS/2018-codelunch`
meetup.com/fr-FR/BrestJS/events/246395747/
(9 janvier 2018)

data-in-space
vs
data-in-time

|                       | Single return value | Multiple return values              |
|-----------------------|---------------------|-------------------------------------|
| Pull<br/>Sync<br/>Interactive | Object              | Iterables (Array, Set, Map, Object) |
| Push<br/>Async<br/>Reactive   | Promise             | Observable                          |

### Promise
one time
one sender
many recipients

### EventEmitter
many times
many senders
many recipients

### Observable (DataStream)
- objects that inform other objects
  of the changes they publish
- Fluent/composable:
  `.map`, `.concat`, `.filter`, `.reduce`
- Observable of Observable: `.flatMap`, `.zip`, `.take`

Discrete vs Continuous
Bacon: EventStream / Property
Reactive(haskell): Events / Behaviors

`A.map(…).filter(…)`

```
A.merge(B)
  .onValue(…)
  .onError(…)
  .onEnd(…)
```

RxJs, BaconJS, Kefir.JS
TC39 Proposal

“Promises are Observables”

Missing slides

Promise
`.then( onSuccess, onError )`

Observable (TC39 Draft 1)
`.subscribe( onNext, onError, onComplete )`

# And Now For Something
# Completely Different

R<sub>x</sub> JS

102 operators!

```
A.pipe(
  map(…),
  filter(…),
  merge(B)
)
```

102 operators

https://rxjs-dev.firebaseapp.com/operator-decision-tree

Things I liked
- multiple Schedulers
- operators encapsulate state
- https://reactive.how/rxjs

Things I didn't like
- not composable
- too. many. operators. in. the. core.

# And Now For Something
# most(.js)ly Different

In my experience
- not-so-simple to write (esp higher-order)
- difficult to debug (`.tap(console.log)`)
- difficult to read/maintain (esp higher-order)

scalability _is_ an issue
⚠ `map (x) => http.get(x)`

→ use the right tool for the right purpose
→ protect downstream (`throttle`/`debounce`)

Examples

`shimaore/abrasive-ducks`
DataStream routing engine
(`red-rings` abstraction)

```javascript
subscribed = subscriptions
  .map( (subscription) => subscription(master_source) )
  .switch()
```

`shimaore/denormalized-invariants`
Invariants sur DB

```coffeescript
invariant 'An order should have a panel link', db, (S) =>
  S
  .filter Business.Order
  .map (doc) =>
    url = "#{panel}/o/#{doc.o}"
    return if doc.panel is url
    doc.panel = url
    await db.put doc
    return
```

```javascript
invariant('An order should have a panel link', db, (S) =>
{ return S
  .filter(Business.Order)
  .map(async(doc) => {
    var url = `${panel}/o/${doc.o}`
    if (doc.panel === url) { return }
    doc.panel = url
    await db.put(doc)
  )}});
```


`shimaore/most-couchdb`
API async-iterable + Node.js Object Stream + most.js

```coffeescript
streamPagination: (params,limit = 10) ->
  done = false
  until done      # iterate until we retrieve all data
    body = null
    until docs?   # iterate until we get a valid response
      docs = await this.getPage(params,limit)
    for doc from docs
      yield doc
    done = docs.length < limit
  return
```
```coffeescript
do ->
  streamableQuery = db.streamPagination selector: account: 'bob'
  for await v from streamableQuery
    console.log v
  return
```

```javascript
streamPagination: async function*(params, limit = 10) {
  var done = false
  while (!done) { // iterate until we retrieve all data
    var body = null; var docs = null
    while (docs == null) { // iterate until we get a valid response
      docs = (await this.getPage(params, limit)) }
    for (var doc of docs) {
      yield doc }
    done = docs.length < limit
  }}});
```
```javascript
(async function() {
  var streamableQuery = db.streamPagination({selector:{account:'bob'}})
  for await (var v of streamableQuery) {
    console.log(v)
  }})()
```

`BrestJS/2019-cs2-surplus`
(WIP)

- generators
- promises
- async functions, await
- async generators
- async iterables vs Node.js `objectMode` Streams
- `S.js` (FRP)

❦ Merci! ❧
==========

Credits: _Joanna Kosinska_, _Todd Quackenbush_,
_Qingbao Meng_, _Hello I m Nik_, _Nicole Y C_,
_Philip Swinburn_, _Joshua Coleman_, _David Gavi_,
_Marcus Lenk_, _Scott Webb_, _Kristopher Roller_, on _Unsplash_
