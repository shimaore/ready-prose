all: clean main.html

clean:
	rm -f main.html

reveal:
	git clone https://github.com/Zenika/zenika-formation-framework.git
	mv zenika-formation-framework/reveal .
	mv zenika-formation-framework/favicon* .
	rm -rf zenika-formation-framework/
	sed -i -e 's/ \.slides / .slide-root /' reveal/theme-zenika/theme.css

main.html: main.md
	cat slide-head.html >$@
	# sed -e 's/&/\&amp\;/g; s/</\&lt\;/g; s/>/\&gt\;/g' $< >>$@
	cat $< >>$@
	cat slide-bottom.html >>$@
